import { Controller } from "@hotwired/stimulus"
import _ from "lodash"

export default class extends Controller {
  connect() {
    const array = [1, 2, 3]
    const doubled = _.map(array, n => n * 2)
    console.log('array', array)  // Output: [1, 2, 3]
    console.log('doubled', doubled)  // Output: [2, 4, 6]
    this.element.textContent = `array=${array} doubled=${doubled.join(', ')}`
  }
}

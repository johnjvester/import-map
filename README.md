# `import-map`

> A simple demonstration of a RESTful API microservice using Rails 7 and Import Maps.

## Publications

This repository is related to the following DZone.com publications:

* TBD

To read more of my publications or browse my public projects, please review one of the following URLs:

* https://dzone.com/authors/johnjvester
* https://gitlab.com/users/johnjvester/projects

## Additional Information

Made with <span style="color:red;">♥</span> &nbsp;by johnjvester@gmail.com, because I enjoy writing code.
